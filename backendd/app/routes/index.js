var express = require("express");
var router = express.Router();

const playersRouter = require("./players");

router.use("/players", playersRouter);

module.exports = router;
