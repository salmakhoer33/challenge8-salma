import { useState } from "react";
import "./App.css";
import Form from "./components/form";
import Names from "./components/form";
import Table from "./Table";
import { Users } from "./users";

function App() {
  const [value, setValue] = useState("");
  const [names, setNames] = useState([]);
  const [name, setName] = useState("");
  const [error, setError] = useState("");
  const [isUpdate, setIsUpdate] = useState(false);
  const [query, setQuery] = useState("");
  const handleSubmit = () => {
    const isFind = names.find((row) => row === value);

    if (!isUpdate) {
      if (!isFind) {
        setNames([...names, value]);
        setValue("");
      } else {
        setError("Name already taken!");
        setTimeout(() => {
          setError("");
        }, 2000);
      }
    } else {
      const newNames = names.map((row) => {
        return row === name ? value : row;
      });
      setName("");
      setNames(newNames);
      setIsUpdate(false);
    }
  };

  const handleChange = (e) => {
    setValue(e.target.value);
  };

  const handleDelete = (name) => {
    const newNames = names.filter((row) => row !== name);

    setNames(newNames);
  };

  const handleUpdate = (name) => {
    setIsUpdate(true);
    setName(name);
    setValue(name);
  };

  const keys = ["username", "email", "experience", "lvl"];

  const search = (data) => {
    return data.filter((item) =>
      keys.some((key) => item[key].toLowerCase().includes(query))
    );
  };

  return (
    <div className="app-container">
      <div className="app-login">
        <input value={value} onChange={handleChange} />
        {value && <button onClick={handleSubmit}>Submit</button>}
        {error && <div className="error">{error}</div>}
        {names.map((row, index) => (
          <Names
            name={row}
            key={index}
            handleDelete={handleDelete}
            handleUpdate={handleUpdate}
          />
        ))}
        <div className="warning">
          {names.length < 1 && <div>Data not exist!</div>}
        </div>
      </div>
      <div className="app-search">
        <input
          type="text"
          placeholder="Search..(lowercase letter) "
          className="search"
          onChange={(d) => setQuery(d.target.value)}
        />
      </div>
      <div className="biodata">
        <Table data={search(Users)} />
      </div>
    </div>
  );
}

export default App;
