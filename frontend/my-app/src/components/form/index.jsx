import react from "react";
import "./style.css";

const Form = ({ name, handleDelete, handleUpdate }) => {
  return (
    <div className="listname-container">
      <p>{name}</p>
      <div className="button">
        <button onClick={() => handleDelete(name)}>Delete</button>
        <button onClick={() => handleUpdate(name)}>Update</button>
      </div>
    </div>
  );
};

export default Form;
