import react from "react";
import "./App.css";

const Table = ({ data }) => {
  return (
    <table>
      <tbody>
        <tr>
          <th>Username</th>
          <th>Email</th>
          <th>Experience</th>
          <th>Lvl</th>
        </tr>
        {data.map((item) => (
          <tr key={item.id}>
            <td>{item.username}</td>
            <td>{item.email}</td>
            <td>{item.experience}</td>
            <td>{item.lvl}</td>
          </tr>
        ))}
      </tbody>
    </table>
  );
};

export default Table;
